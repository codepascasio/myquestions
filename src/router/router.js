import VueRouter from 'vue-router'
import game from '../components/game.vue'
import home from '../components/home.vue'
import options from '../components/options.vue'

import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);
const routes = [
    { path: '/', component: home },
    {path: '/game/:asignatura', component: game},
    {path: '/options', component: options}
  ]
const router = new VueRouter({
    routes // short for `routes: routes`
  })

export default router;